from django.forms.widgets import CheckboxSelectMultiple
from django.forms import ModelForm
from .models import Category, Post, Comment

class PlaceForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'info',
            'picture',
        ]
        labels = {
            'name': 'Lugar',
            'info': 'Descrição',
            'picture': 'Imagem (URL)',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Nome',
            'text': 'Comentário',
        }

class CategoryForm(ModelForm):    
    class Meta:
        model = Category
        fields = [
            'name',
            'info',
            'places'
        ]
        labels = {
            'name'   : 'Região',
            'info'   : 'Descrição',
            'places' : 'Lugares',
        }
    
    # Many-to-many: lista de itens para checkboxes
    def __init__(self, *args, **kwargs):        
        super(CategoryForm, self).__init__(*args, **kwargs)
        
        self.fields["places"].widget = CheckboxSelectMultiple()
        self.fields["places"].queryset = Post.objects.all()
