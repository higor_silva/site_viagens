from django.views import generic
from django.urls import reverse_lazy

from .models import Post, Category
from .forms import CategoryForm, PlaceForm, CommentForm


# ====== #
# CREATE #
# ====== #

class PlaceCreateView(generic.CreateView):
    form_class = PlaceForm
    template_name = 'places/create.html'
    context_object_name = 'place'

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse_lazy('places:detail', args=(self.object.id, ))

class CommentCreateView(generic.CreateView):
    form_class = CommentForm
    template_name = 'places/comment.html'
    context_object_name = 'commment'

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse_lazy('places:detail', kwargs={'pk' : pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs) # context padrão
        context['place'] = Post.objects.get(pk=self.kwargs.get('pk')) # Adiciona o post ao context
        return context
    
    def form_valid(self, form):
        form.instance.place_id = self.kwargs.get('pk')
        return super().form_valid(form)

class CategoryCreateView(generic.CreateView):
    form_class = CategoryForm
    template_name = 'places/create_category.html'

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse_lazy('places:detail-category', args=(self.object.id, ))

# ==== #
# READ #
# ==== #

class PlaceDetailView(generic.DetailView):
    model = Post
    template_name = 'places/detail.html'
    context_object_name = 'place'

class PlaceListView(generic.ListView):
    model = Post
    template_name = 'places/index.html'
    context_object_name = 'places_list'

class PlaceSearchView(generic.ListView):
    model = Post
    template_name = 'places/search.html'
    context_object_name = 'places_list'

    def get_queryset(self):
        search = self.request.GET.get('place_name', '')
        return Post.objects.filter(name__icontains=search)

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'places/detail_category.html'
    context_object_name = 'category'

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'places/categories.html'
    context_object_name = 'categories_list'

# ====== #
# UPDATE #
# ====== #

class PlaceUpdateView(generic.UpdateView):
    model = Post
    template_name = 'places/update.html'
    context_object_name = 'place'
    fields = ['name', 'info', 'picture']

# ====== #
# DELETE #
# ====== #

class PlaceDeleteView(generic.DeleteView):
    model = Post
    template_name = 'places/delete.html'
    context_object_name = 'place'
    success_url = reverse_lazy('places:index')

class CategoryDeleteView(generic.DeleteView):
    model = Category
    template_name = 'places/delete_category.html'
    context_object_name = 'category'
    success_url = reverse_lazy('places:categories')
