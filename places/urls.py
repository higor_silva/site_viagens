from django.urls import path

from . import views

app_name = 'places'
urlpatterns = [
    path('',                 views.PlaceListView.as_view(),   name='index'),
    path('create/',          views.PlaceCreateView.as_view(), name='create'),
    path('search/',          views.PlaceSearchView.as_view(), name='search'),
    path('<int:pk>/',        views.PlaceDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.PlaceUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.PlaceDeleteView.as_view(), name='delete'),

    path('<int:pk>/comment/', views.CommentCreateView.as_view(), name='comment'),

    path('categories/',                 views.CategoryListView.as_view(),   name='categories'),
    path('categories/<int:pk>/',        views.CategoryDetailView.as_view(), name='detail-category'),
    path('categories/create',           views.CategoryCreateView.as_view(), name='create-category'),
    path('categories/delete/<int:pk>/', views.CategoryDeleteView.as_view(), name='delete-category'),
]
